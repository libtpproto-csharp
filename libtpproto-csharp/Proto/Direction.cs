/*
 * License: GPLv3+
 * Created at 15:14 17.05.2008
 * 
 * Author: Marcel Hauf 
 */

using System;

namespace LibTpProto.Proto
{
	/// <summary>
	/// Description of Direction.
	/// </summary>
	public enum Direction : byte
	{
		any,
		client,
		server
	}
}
