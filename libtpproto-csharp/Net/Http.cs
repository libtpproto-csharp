/*
 * License: GPLv3+
 * Created at 18:07 19.05.2008
 * 
 * Author: Marcel Hauf
 */

using System;

namespace LibTpProto.Net
{
	/// <summary>
	/// Description of Http.
	/// </summary>
	public class Http
	{
		public string ipAdress;
		public const TypeOfConnection ConnectionType = TypeOfConnection.Http;
		
		public Http()
		{
			// TODO: Http tunneling
		}
	}
}
