/*
 * License: GPLv3+
 * Created at 19:09 19.05.2008
 * 
 * Author: Marcel Hauf
 */

using System;

namespace LibTpProto.Net
{
	/// <summary>
	/// Interface for a connection.
	/// </summary>
	public interface IConnection
	{
		bool Connect();
		bool Disconnect();
		bool Write(string message);
		// TODO: Event for new message 
	}
}
