/*
 * License: GPLv3+
 * Created at ${TIME} ${DATE}
 * 
 * Author: Marcel Hauf 
 */

using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;

namespace LibTpProto.Net
{
	/// <summary>
	/// Description of TcpConnection.
	/// </summary>
	public class Tp
	{
		private Socket netSocket;
		private StreamHandler streamHandler;
		
		public int Port = 6923;
		public string ipAdress;
		public const TypeOfConnection ConnectionType = TypeOfConnection.Tp;
		
		public void Connect()
		{
			Connect(this.ipAdress, this.Port);
		}
		
		public void Connect(string ipAdress)
		{
			Connect(ipAdress, this.Port);
		}
		
		public void Connect(string ipAdress, int port)
		{
			netSocket = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			try
			{
				netSocket.Connect(ipAdress, port);
				streamHandler = new StreamHandler(netSocket);
			}
			catch (SocketException se)
			{
				Console.WriteLine("Error while connecting to server with tcp connection: " + se);
				// TODO: Error handling
			}
		}
	}
}
