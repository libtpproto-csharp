/*
 * License: GPLv3+
 * Created at ${TIME} ${DATE}
 * 
 * Author: Marcel Hauf 
 */

using System;
using System.Net;
using System.Net.Sockets;

namespace LibTpProto.Net
{
	/// <summary>
	/// Description of NetHandler.
	/// </summary>
	public class NetHandler
	{
		TypeOfConnection ConnectionType = TypeOfConnection.Tp;
		
		public NetHandler(TypeOfConnection connectionType)
		{
			this.ConnectionType = connectionType;
			
			switch (ConnectionType)
			{
				case TypeOfConnection.Tp:
					break;
				default:
					// TODO: Error handling: unsupported connection type
					break;
			}
		}
	}
	
	public enum TypeOfConnection : byte
	{
		Tp,
		Tps,
		Http,
		Https
	}
}
