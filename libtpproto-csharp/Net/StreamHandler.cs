/*
 * User: Agon
 * Date: 26.04.2008
 * Time: 18:17
 * 
 * Copyright Marcel Hauf
 */

using System;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.Text;
using System.Threading;

namespace LibTpProto.Net
{
	/// <summary>
	/// Description of Connection.
	/// </summary>
	public class StreamHandler
	{
		Stream netStream;
		StreamWriter netWriter;
		public int ReadBuffer = 1024;
		
		public StreamHandler(Socket netSocket)
		{
			this.netStream = new NetworkStream(netSocket);
			this.netWriter = new StreamWriter(netStream);
			GoReading();
		}
		
		public void Write(string message)
		{
			netWriter.WriteLine(message);
		}
		
		private void GoReading()
		{
			byte[] ByteBuffer = new byte[this.ReadBuffer];
			netStream.BeginRead(ByteBuffer, 0, ByteBuffer.Length
			                    , new AsyncCallback(HasRead), netStream);
		}
		private void HasRead(IAsyncResult ar)
		{
			NetworkStream inNetStream = (NetworkStream)ar.AsyncState;
			byte[] readBuffer=new byte[1024];
			string received=String.Concat(Encoding.Unicode.GetString(
				readBuffer,0,readBuffer.Length));

			while(inNetStream.DataAvailable)
			{
				inNetStream.BeginRead(readBuffer,0,readBuffer.Length
				                      ,new AsyncCallback(HasRead)
				                      ,inNetStream);
			}
			// Restart reading of the stream
			GoReading();
			// Trigger an read event
			OnRead(new ReadEventArgs(received));

		}
		#region ReadEvent
		public event EventHandler<ReadEventArgs> OnReadEvent;
		
		protected virtual void OnRead(ReadEventArgs rea)
		{
			if (OnReadEvent != null)
			{
				OnReadEvent(this, rea);
			}
		}
	}
	
	public class ReadEventArgs : EventArgs
	{
		public ReadEventArgs(string message)
		{
			this.message = message;
		}
		public string Message
		{
			get
			{
				return(message);
			}
		}
		string message;
	}
	#endregion ReadEvent
}
