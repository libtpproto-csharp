/*
 * License: GPLv3+
 * Created at ${TIME} ${DATE}
 * 
 * Author: Marcel Hauf 
 */

using System;
using System.CodeDom;
using System.Xml;
using System.Collections;

namespace CodeGenerator
{
	/// <summary>
	/// Description of Variables.
	/// </summary>
	public class TypeHandler
	{
		/// <summary>
		/// Adds a class to the namespace.
		/// </summary>
		/// <param name="cn">The namespace there the class gets added.</param>
		/// <param name="reader">The sub tree where the class is descript.</param>
		/// <param name="name">The name of the class</param>
		public void AddClass(CodeNamespace cn, XmlReader reader, string name)
		{
			CodeTypeDeclaration ctd = new CodeTypeDeclaration(name);
			ctd.IsClass = true;
			ctd.Attributes = MemberAttributes.Public;
			while(reader.Read())
			{
				if(reader.Depth == 1 && reader.NodeType == XmlNodeType.Element)
				{
					AddVars(ctd, reader.ReadSubtree());
				}
			}
			cn.Types.Add(ctd);
		}
		
		/// <summary>
		/// Add a variables to the code type declaration.
		/// </summary>
		/// <param name="ctd">The code type declaration there the variables gets added.</param>
		/// <param name="reader">The sub tree where the variables are descript.</param>
		public void AddVars(CodeTypeDeclaration ctd, XmlReader reader)
		{
			string varType = "";
			string name = "";
			CodeMemberProperty cmp = new CodeMemberProperty();
			CodeMemberField cmf = new CodeMemberField();
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && reader.Depth == 0)
				{
					varType = reader.Name;
					if(varType == "enumeration")
					{
						AddEnum(ctd, reader.ReadSubtree());
						break;
					}
				}
				if(reader.Name == "name")
				{
					name = reader.ReadString();
				}
				if(reader.Name == "description")
				{
					cmp.Comments.Add(new CodeCommentStatement("<summary>", true));
					cmp.Comments.Add(new CodeCommentStatement(reader.ReadString(), true));
					cmp.Comments.Add(new CodeCommentStatement("</summary>", true));
				}
			}
			if(varType != "enumeration")
			{
				switch (varType)
				{
					case "integer":
						cmp.Type = new CodeTypeReference("int");
						cmf.Type = new CodeTypeReference("int");
						break;
					case "character":
						cmp.Type = new CodeTypeReference("string");
						cmf.Type = new CodeTypeReference("string");
						break;
					case "string":
						cmp.Type = new CodeTypeReference("string");
						cmf.Type = new CodeTypeReference("string");
						break;
					default:
						Console.WriteLine("Unsupported variable type: " + varType);
						break;
				}
				
				cmf.Name = "m_" + name;
				cmf.Attributes = MemberAttributes.Private | MemberAttributes.Final;
				ctd.Members.Add(cmf);
				
				cmp.Name = name;
				cmp.Attributes = MemberAttributes.Public | MemberAttributes.Final;
				cmp.HasGet = true;
				cmp.GetStatements.Add(new CodeSnippetExpression("return m_" + name));
				cmp.HasSet = true;
				cmp.SetStatements.Add(new CodeSnippetExpression("m_" + name + " = value"));
				ctd.Members.Add(cmp);
			}
		}
		
		/// <summary>
		/// Adds an enum to the code type declaration.
		/// </summary>
		/// <param name="ctd">The code type declaration which gets the new enum.</param>
		/// <param name="reader">The sub tree where the enum is descript.</param>
		private void AddEnum(CodeTypeDeclaration ctd, XmlReader reader)
		{
			CodeTypeDeclaration enumCtd = new CodeTypeDeclaration();
			enumCtd.IsEnum = true;
			
			string enumType = "unsigned";
			string name = "";
			int size = 32;
			
			
			
			while(reader.Read())
			{
				enumType = reader.GetAttribute("type");
				size = int.Parse(reader.GetAttribute("size"));
				if(reader.Name == "name")
				{
					name = reader.ReadString();
				}
				if(reader.Name == "value")
				{
					CodeMemberField field = new CodeMemberField();
					field.Name = reader.GetAttribute("Protocol");
					enumCtd.Members.Add(field);
				}
			}
			ctd.Members.Add(enumCtd);
		}
		
		// TODO: List is missing
	}
}
