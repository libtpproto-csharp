/*
 * License: GPLv3+
 * Created at 12:41 17.05.2008
 * 
 * Author: Marcel Hauf
 */

using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Reflection;
using System.CodeDom;
using Microsoft.CSharp;
using Microsoft.VisualBasic;
using Microsoft.JScript;
using Refly;
using Refly.CodeDom;
using Refly.CodeDom.Expressions;
using Refly.CodeDom.Collections;

namespace TPCodeGenerator
{
	/// <summary>
	/// Thousand parsec protocol source code generator.
	/// </summary>
	public class Generator
	{
		/// <summary>
		/// Namespace name of the will be generated code.
		/// </summary>
		public string Namespace = "LibTpProto.Proto";
		
		/// <summary>
		/// Out put path for the source code.
		/// Generator automatically adds the version to string.
		/// </summary>
		private string outPutPath = ".";
		
		/// <summary>
		/// Path to the xml file.
		/// </summary>
		private string xmlFile = "./protocol";

		/// <summary>
		/// Overall namespace for the protocol library.
		/// Depends on the version of the protocol library.
		/// </summary>
		private NamespaceDeclaration nd;
		
		/// <summary>
		/// Language of the source code.
		/// </summary>
		public Language language = Language.CSharp;
		
		/// <summary>
		/// Thousand parsec protocol source code generator.
		/// </summary>
		/// <param name="xmlFile">The file path to the xml file.</param>
		/// <param name="outputPath">The directory path to the source code.</param>
		/// <param name="language">Language of the source code.</param>
		public void GenerateCode(string xmlFile, string outputPath, Language language)
		{
			// Setup variables
			this.outPutPath = outputPath;
			this.xmlFile = xmlFile;
			this.language = language;
			XmlReader reader;
			
			// Setup in/out put
			if(File.Exists(xmlFile))
			{
				reader = new XmlTextReader (xmlFile);
			}
			else
			{
				throw(new Exception("Could not found xml file."));
			}
			
			while(reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element: //This node is a element
						switch (reader.Depth)
						{
							case 0: //protocol
								string version = reader.GetAttribute("version");
								Namespace = Namespace + "." + version;
								if(version != null)
								{
									outPutPath = "./" + version;
									Directory.CreateDirectory(outPutPath);
								}
								break;
							case 1: //parameterset and packet
								Console.Write("Creating class for "
								              + reader.GetAttribute("name")
								              + "...");
								GenerateClass(reader.ReadSubtree());
								break;
							case 2:
								break;
							default:
								break;
						}
						break;
					case XmlNodeType.Text: //Display the text from each element
						break;
					case XmlNodeType.EndElement: //Display the end of a element
						break;
				}
			}
			CleanCode(); // Cleans code from @ chars, this is a workaround
		}
		
		private void GenerateClass(XmlReader reader)
		{
			// --- Starts generating code ---
			nd = new NamespaceDeclaration(Namespace);
			
			ClassDeclaration cd = null;
			
			//Create Class Using Statements

			nd.Imports.Add("System");
			
			while(reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						if(reader.Name == "packet" || reader.Name == "parameterset")
						{
							cd = nd.AddClass(reader.GetAttribute("name"));
							cd.Attributes = TypeAttributes.Class | TypeAttributes.Public;
							string id = reader.GetAttribute("id");
							if(id != null && id != "")
							{
								FieldDeclaration fied = cd.AddField(typeof(byte), "FRAME_TYPE");
								fied.Attributes = MemberAttributes.Const | MemberAttributes.Public | MemberAttributes.Final;
								fied.InitExpression = Expr.Snippet(id);
							}
						}
						if(reader.GetAttribute("base") != "")
						{
							switch (reader.GetAttribute("base"))
							{
								case "Header":
									cd.Parent = new TypeTypeDeclaration(typeof(Header));
									break;
								case "Response":
									cd.Parent = new TypeTypeDeclaration(typeof(Response));
									break;
								case "Request":
									cd.Parent = new TypeTypeDeclaration(typeof(Request));
									break;
							}
						}
						if(reader.Depth == 1)
						{
							switch (reader.Name)
							{
								case "description":
									cd.Doc.Summary.AddText(CommentFormatter(reader.ReadString()), "");
									break;
								case "direction":
									ConstantDeclaration cond = cd.AddConstant(
										typeof(LibTpProto.Proto.Direction)
										, "direction"
										, new SnippetExpression("LibTpProto.Proto.Direction."
										                        + reader.ReadString()));
									cond.Attributes = MemberAttributes.Public | MemberAttributes.Final;
									break;
								case "structure":
									AddVars(reader.ReadSubtree(), cd);
									break;
								default:
									break;
							}
						}
						break;
					case XmlNodeType.Text: //Display the text from each element
						break;
					case XmlNodeType.EndElement: //Display the end of a element
						break;
				}
			}
			AddToString(cd);
			
			Refly.CodeDom.CodeGenerator cg = new Refly.CodeDom.CodeGenerator();
			cg.CreateFolders = false;
			cg.Copyright = @"
  License: GPLv3+
  Created at 12:41 17.05.2008
  
  Author: Marcel Hauf";
			switch (this.language)
			{
				case Language.CSharp:
					cg.Provider = new CSharpCodeProvider();
					break;
				case Language.VisualBasic:
					cg.Provider = new VBCodeProvider();
					break;
				case Language.JScript:
					cg.Provider = new JScriptCodeProvider();
					break;
				default:
					throw(new Exception("Unknown language."));
			}
			cg.GenerateCode(outPutPath, nd);
			
			Console.Write("done!");
			Console.WriteLine();
		}

		private void AddVars(XmlReader reader, ClassDeclaration cd)
		{
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && reader.Depth == 1)
				{
					AddVar(reader.ReadSubtree(), cd);
				}
			}
		}

		private void AddVar(XmlReader reader, ClassDeclaration cd)
		{
			string varType = "";
			string typeOf = "";
			string size = "32";
			string name = "";
			string longname = "";
			string comment = "";
			string example = "";
			string note = "";
			
			FieldDeclaration fd = null;
			PropertyDeclaration pd = null;

			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element
				   && reader.Depth == 0
				   && reader.NodeType != XmlNodeType.EndElement
				   && fd == null)
				{
					varType = reader.Name;
					typeOf = reader.GetAttribute("type");
					
					size =reader.GetAttribute("size");
					
					switch (varType)
					{
						case "enumeration":
							AddEnum(reader.ReadSubtree(), cd);
							break;
						case "list":
							AddList(reader.ReadSubtree(), cd);
							break;
					}
				}
				else if(reader.NodeType == XmlNodeType.Element && reader.Depth == 1)
				{
					switch (reader.Name)
					{
						case "name":
							name = reader.ReadString();
							break;
						case "longname":
							longname = "Longname: " + reader.ReadString();
							break;
						case "note":
							note = "Note: " + reader.ReadString();
							break;
						case "description":
							comment = reader.ReadString();
							break;
						case "example":
							example = "Example: " + reader.ReadString();
							break;
					}
				}
			}
			switch (varType)
			{
				case "integer":
					switch (typeOf)
					{
						case "signed":
							switch (size)
							{
								case "8": // sbyte
									fd = cd.AddField(typeof(sbyte), name);
									break;
								case "32": // normal int
									fd = cd.AddField(typeof(int), name);
									break;
								case "64": // long
									fd = cd.AddField(typeof(long), name);
									break;
							}
							break;
						case "unsigned":
							switch (size)
							{
								case "8": // byte
									fd = cd.AddField(typeof(byte), name);
									break;
								case "32": // uint
									fd = cd.AddField(typeof(uint), name);
									break;
								case "64": // ulong
									fd = cd.AddField(typeof(ulong), name);
									break;
							}
							break;
					}
					break;
				case "character":
					fd = cd.AddField("string", name);
					break;
				case "string":
					fd = cd.AddField("string", name);
					break;
			}
			if(fd != null)
			{
				pd = cd.AddProperty(fd, name, true, true, true);
				pd.Doc.Summary.AddText(CommentFormatter(comment), "");
				pd.Doc.Summary.AddText(CommentFormatter(note), "");
				pd.Doc.Summary.AddText(CommentFormatter(example), "");
				pd.Attributes = MemberAttributes.Public | MemberAttributes.Final;
				
				fd.Attributes = MemberAttributes.Private;
			}
		}
		
		/// <summary>
		/// Adds a enum to the class declaration.
		/// </summary>
		/// <param name="reader">The xmlreader which provides the informations.</param>
		/// <param name="cd">The class declaration which gets the enum </param>
		private void AddEnum(XmlReader reader, ClassDeclaration cd)
		{
			EnumDeclaration ed = null;
			string name = "name";
			string typeOf = "int";
			int size = 32;
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element)
				{
					switch (reader.Name)
					{
						case "enumeration":
							typeOf = reader.GetAttribute("type");
							size = int.Parse(reader.GetAttribute("size"));
							break;
						case "name":
							name = reader.ReadString();
							ed = nd.AddEnum(name, false);
							break;
						case "longname":
							ed.Doc.Summary.AddText(CommentFormatter("Longname: " + reader.ReadString())
							                       , "");
							break;
						case "description":
							ed.Doc.Summary.AddText(CommentFormatter(reader.ReadString()), "");
							break;
						case "value":
							string id = reader.GetAttribute("id");
							FieldDeclaration fd = ed.AddField(reader.GetAttribute("name"));
							fd.Doc.Summary.AddText(CommentFormatter(reader.ReadString()), "");
							fd.Attributes = MemberAttributes.Public | MemberAttributes.Final;
							fd.InitExpression = Expr.Snippet(id);
							break;
						case "note":
							ed.Doc.Summary.AddText(CommentFormatter(reader.ReadString()));
							break;
					}
				}
			}

			FieldDeclaration fieldd = cd.AddField(name, name);
			
			PropertyDeclaration pd = cd.AddProperty(fieldd, true, true, true);
			pd.Attributes = MemberAttributes.Public | MemberAttributes.Final;
			
			PropertyDeclaration pdi = cd.AddProperty(typeof(int), "Get" + name);
			pdi.Attributes = MemberAttributes.Public | MemberAttributes.Final;
			pdi.Get.Return(Expr.Cast("int", Expr.This.Field(fieldd)));
			
			/*
			MethodDeclaration md = cd.AddMethod(name.Substring(0,1).ToUpper() + name.Substring(1));
			md.Attributes = MemberAttributes.Public | MemberAttributes.Final;
			md.Signature.ReturnType = new TypeTypeDeclaration(Type.GetType(name));
			md.Body.AddAssign(Expr.This.Field(fieldd), Expr.Value);
			md.Body.Return(Expr.This.Field(fieldd));
			 */
		}
		
		/// <summary>
		/// Adds a list to the class declaration based on the xmlreader input.
		/// </summary>
		/// <param name="reader">Xmlreader which provides the informations for the list.</param>
		/// <param name="cd">Class declaration which gets the list included.</param>
		private void AddList(XmlReader reader, ClassDeclaration cd)
		{
			string name = "";
			string longname = "";
			string description = "";
			
			cd.Imports.Add("System.Collections.Generic");
			ClassDeclaration cld = null;
			
			while(reader.Read())
			{
				if(reader.NodeType == XmlNodeType.Element && reader.Depth == 1)
				{
					switch (reader.Name)
					{
						case "name":
							name = reader.ReadString();
							break;
						case "longname":
							longname = reader.ReadString();
							break;
						case "description":
							description = reader.ReadString();
							break;
						case "structure":
							string newName = "List" + name;

							newName = newName.Substring(0,1).ToUpper() + newName.Substring(1);
							
							cld = cd.AddClass(newName);
							cld.Attributes = TypeAttributes.Public;
							cld.Doc.Summary.AddText(CommentFormatter("Longname: " + longname));
							cld.Doc.Summary.AddText(CommentFormatter(description));
							
							FieldDeclaration fd = cd.AddField("List<" + newName + ">", name);
							PropertyDeclaration pd = cd.AddProperty(fd, "L" + name, true, true, true);
							pd.Attributes = MemberAttributes.Public | MemberAttributes.Final;
							
							AddVars(reader.ReadSubtree(), cld);
							break;
					}
				}
			}
			AddToString(cld);
		}
		
		private void AddToString(ClassDeclaration cd)
		{
			// Overrides ToString() methods
			Expression expres;
			string lName = "";
			bool isList = false;

			expres = Expr.Prim("");

			foreach(PropertyDeclaration p in cd.Properties)
			{
				
				switch (p.Type.Name.Substring(0,3)) // Only the first 4 chars
				{
					case "sby":
					case "Byt":
						expres += Expr.Prim(">n<") + Expr.This.Prop(p).Method("ToString").Invoke();
						break;
					case "uin":
					case "Int":
						expres += Expr.Prim("<n>") + Expr.This.Prop(p).Method("ToString").Invoke();
						break;
					case "lon":
					case "UIn":
						expres += Expr.Prim("<<n>>") + Expr.This.Prop(p).Method("ToString").Invoke();
						break;
					case "str":
						expres += Expr.This.Prop(p);
						break;
					case "Lis":
						lName = p.Name;
						break;
						/*
					default:
						Console.WriteLine();
						Console.WriteLine("Class name: " + cd.Name);
						Console.WriteLine("Property name: " + p.Name + " Type of property: " + p.Type.Name);
						break;
						 */
				}
			}
			if(isList)
			{
				expres += Expr.Prim("]");
			}
			if(expres != null) // TODO: Support more than one List in a class
			{
				MethodDeclaration md = cd.AddMethod("ToString");
				md.Attributes = MemberAttributes.Public | MemberAttributes.Override;
				md.Signature.ReturnType = new TypeTypeDeclaration(typeof(string));
				if(lName != "")
				{
					
					md.Body.Add(Stm.Var("string"
					                    , "liststring"
					                    , Expr.Prim(@"@<")
					                    + Expr.This.Field(lName).Method("Count")
					                    + Expr.Prim(@"@>[")));
					md.Body.Add(Stm.Var("int", "count", Expr.Prim(0)));
					md.Body.Add(Expr.Snippet("foreach(List" + lName.Substring(1) + " list in " + lName + ")\n"
					                         + "\t\t\t{\n"
					                         + "\t\t\t\tif(count > 1 && count > " + lName + ".Count)\n"
					                         + "\t\t\t\t{\n"
					                         + "\t\t\t\t\t" + @"liststring += "","" + list.ToString();" + "\n"
					                         + "\t\t\t\t}\n"
					                         + "\t\t\t\telse\n"
					                         + "\t\t\t\t{\n"
					                         + "\t\t\t\t\t" + @"liststring += list.ToString();"
					                         + "\n\t\t\t\t}\n"
					                         + "\t\t\t\tcount++;\n"
					                         + "\t\t\t}"));
					md.Body.Add(Expr.Snippet(@"liststring += """ + "]" + @""""));
					expres += Expr.Snippet("liststring");
				}
				md.Body.Add(Stm.Return(expres));
			}
		}

		/// <summary>
		/// Workaround for the @ chars
		/// </summary>
		private void CleanCode()
		{
			Console.WriteLine("Cleaning all code from @ chars in directory " + outPutPath);
			
			foreach(string s in Directory.GetFiles(outPutPath))
			{
				Console.Write("Replacing @ chars in " + s + ".");
				TextReader tr = new StreamReader(new FileStream(s, FileMode.Open));
				string input = tr.ReadToEnd();
				input = input.Replace("@", "");
				tr.Close();
				
				Console.Write('.');
				
				TextWriter tw = new StreamWriter(new FileStream(s, FileMode.Create));
				tw.Write(input);
				tw.Close();
				
				Console.Write(".done!\n");
			}
		}
		
		/// <summary>
		/// Adds a break to the input string at the length of 100 chars.
		/// </summary>
		/// <param name="input">Input string which gets formatted.</param>
		/// <returns>The formatted string.</returns>
		private string CommentFormatter(string input)
		{
			return CommentFormatter(input, 100);
		}
		
		/// <summary>
		/// Adds a break to the input string at the given length.
		/// </summary>
		/// <param name="input">Input string which gets formatted.</param>
		/// <param name="drimLength">The length at which the string gets a "cut".</param>
		/// <returns>The formatted string.</returns>
		private string CommentFormatter(string input, int drimLength)
		{
			if(input.Length > drimLength)
			{
				string buffer = "empty";
				for(int i=0; i>=input.Length;i+=drimLength)
				{
					buffer += input.Substring(i, i+drimLength) + "\n";
				}
				return buffer;
			}
			else
			{
				if(input == "")
				{
					return input;
				}
				else
				{
					return input + " \n";
				}
			}
		}
	}
	
	/// <summary>
	/// Thousand parsec .Net language support.
	/// </summary>
	public enum Language : byte
	{
		CSharp,
		VisualBasic,
		JScript,
	}
}



// Fake objects
public class Header{}
public class Response{}
public class Request{}

public enum code{}
public enum mask{}
public enum feature{}
public enum filter{}
public enum paramid{}
public enum reason{}
