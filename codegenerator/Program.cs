/*
 * License: GPLv3+
 * Created at ${TIME} ${DATE}
 * 
 * Author: Marcel Hauf 
 */
using System;

namespace TPCodeGenerator
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("Generating tpproto-csharp code...");
			// TODO: Add support for different languages (CSharp and VisualBasic)
			Generator cg = new Generator();
			cg.GenerateCode("protocol.xml", ".", Language.CSharp);
			
			Console.Write("Press any key to continue . . . ");
			Console.ReadKey(true);
		}
	}
}
