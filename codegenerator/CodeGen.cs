/*
 * License: GPLv3+
 * Created at ${TIME} ${DATE}
 * 
 * Author: Marcel Hauf 
 */

using System;
using System.IO;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Serialization;
using System.Collections;
using System.CodeDom;
using System.CodeDom.Compiler;
using Microsoft.CSharp;
using System.Reflection;

namespace CodeGenerator
{
	/// <summary>
	/// Loads the xml file.
	/// </summary>
	public class CodeGenerator
	{
		/// <summary>
		/// Namespace name of the will be generated code.
		/// </summary>
		public string Namespace = "LibTpProto.Proto";
		
		/// <summary>
		/// An arraylist of strings which will be included as using statements.
		/// Eg. string: System
		/// </summary>
		public ArrayList UsingStatements = new ArrayList();

		private static TypeHandler typeHandler = new TypeHandler();
		
		private string outPutPath = ".";
		
		private string xmlFile = "./protocol";
		
		private XmlTextReader reader;
		
		private TextWriter sourceWriter;
		
		private ArrayList classList = new ArrayList();
		
		/// <summary>
		/// This will generate the code.
		/// </summary>
		/// <param name="xmlFile">Path to the xml file.</param>
		/// <param name="outputPath">Out put path.</param>
		public CodeGenerator(string xmlFile, string outputPath)
		{
			// TODO: Add choose which language should be used
			// Setup variables
			this.outPutPath = outputPath;
			this.xmlFile = xmlFile;
			
			// Setup in/out put
			if(File.Exists(xmlFile))
			{
				reader = new XmlTextReader (xmlFile);
			}
			else
			{
				throw(new Exception("Could not found xml file."));
			}
			
			// TODO: Choose the language of the code gen
			CSharpCodeGen();
		}
		
		private void CSharpCodeGen()
		{
			//Setup code generators for csharp code
			CSharpCodeProvider provider = new CSharpCodeProvider();
			CodeGeneratorOptions options = new CodeGeneratorOptions();
			
			while (reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element: //This node is a element
						switch (reader.Depth)
						{
							case 0: //protocol
								string version = reader.GetAttribute("version");
								if(version != null)
								{
									outPutPath += "/" + version;
									Directory.CreateDirectory(outPutPath);
								}
								break;
							case 1: //parameterset and packet
								string name = reader.GetAttribute("name");
								if(name == "OrderParams")
								{
									GenerateParaSet(provider, options, reader.ReadSubtree());
								}
								else if ( name == "Header")
								{
									GenerateHeader(provider, options, reader.ReadSubtree());
								}
								break;
							case 2:
								break;
							default:
								break;
						}
						break;
					case XmlNodeType.Text: //Display the text from each element
						break;
					case XmlNodeType.EndElement: //Display the end of a element
						break;
				}
			}
			reader.Close();
		}
		
		private void GenerateHeader(CodeDomProvider provider
		                            , CodeGeneratorOptions options
		                            , XmlReader reader)
		{
			sourceWriter = new StreamWriter(outPutPath + "/Header.cs");
			// --- Starts generating code ---
			
			//Declare Using Statements
			UsingStatements.Add("System");
			
			//Setup compile unit
			CodeCompileUnit ccu = new CodeCompileUnit();
			
			//Create Class Namespaces
			CodeNamespace codeNamespace = new CodeNamespace(Namespace);
			
			//Create Class Using Statements
			foreach(string s in UsingStatements)
			{
				codeNamespace.Imports.Add(new CodeNamespaceImport(s));
			}
			
			while(reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						if(reader.Name == "structure")
						{
							Console.WriteLine("Debug: structure will be created.");
							typeHandler.AddClass(codeNamespace, reader.ReadSubtree(), "Header");
						}
						break;
					case XmlNodeType.Text: //Display the text from each element
						break;
					case XmlNodeType.EndElement: //Display the end of a element
						break;
				}
			}			
			ccu.Namespaces.Add(codeNamespace);
			provider.GenerateCodeFromCompileUnit(ccu, sourceWriter, options);
			sourceWriter.Close();
		}
		
		
		// TODO: Recode everthing below
		private void GenerateParaSet(CodeDomProvider provider
		                             , CodeGeneratorOptions options
		                             , XmlReader reader)
		{
			sourceWriter = new StreamWriter(outPutPath + "/OrderParams.cs");
			// --- Starts generating code ---
			
			//Declare Using Statements
			UsingStatements.Add("System");
			
			//Setup compile unit
			CodeCompileUnit ccu = new CodeCompileUnit();
			
			//Create Class Namespaces
			CodeNamespace codeNamespace = new CodeNamespace(Namespace);
			
			//Create Class Using Statements
			foreach(string s in UsingStatements)
			{
				codeNamespace.Imports.Add(new CodeNamespaceImport(s));
			}
			CodeTypeDeclaration ctd = new CodeTypeDeclaration("OrderParams");
			string comments = "";
			while(reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						switch (reader.Depth)
						{
							case 0:
								break;
							case 1: //parameter and name and description
								switch (reader.Name)
								{
										//Does not work
									case "longname":
										comments = reader.Value;
										break;
									case "description":
										comments += reader.Value;
										break;
										// Works
									case "parameter":
										GenerateParameter(reader.ReadSubtree(), ctd, reader.GetAttribute("name"));
										break;
									default:
										break;
								}
								
								break;
							default:
								break;
						}
						break;
					case XmlNodeType.Text: //Display the text from each element
						break;
					case XmlNodeType.EndElement: //Display the end of a element
						break;
				}
			}
			Console.WriteLine(comments);
			ctd.Comments.Add(new CodeCommentStatement(comments));
			codeNamespace.Types.Add(ctd);
			
			ccu.Namespaces.Add(codeNamespace);
			provider.GenerateCodeFromCompileUnit(ccu, sourceWriter, options);
			sourceWriter.WriteLine();
			sourceWriter.Close();
		}
		
		private void GenerateParameter(XmlReader reader, CodeTypeDeclaration ctd, string name)
		{
			CodeTypeDeclaration pclass = new CodeTypeDeclaration(name);
			while(reader.Read())
			{
				switch (reader.NodeType)
				{
					case XmlNodeType.Element:
						switch (reader.Depth)
						{
							case 0:
								break;
							case 1:
								break;
							default:
								break;
						}
						break;
					default:
						break;
				}
			}
			ctd.Members.Add(pclass);
		}
	}
}
